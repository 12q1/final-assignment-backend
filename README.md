# Final Assignment Week 8 - Back end server

This server uses koa for database management and socketIO for realtime 2 way communication between client and server. It was written mostly in typescript, a script in the package.json file causes the server to compile to JS (in ./target) when deployed.

It can be started by navigating to the root directory and entering 'yarn start'. 

When running the server runs on localhost:4000 but will take environment variables if they are available.

The server creates 3 tables (users, events, tickets) and provides a number of endpoints to allow retrieval, creation and modification. 

Users are initially authenticated at a signup/login endpoint and they are issued a jwt token that is valid for 4 hours and stored locally on the client device. 

Users permissions are limited to certain actions through the Authorized() validator. 

Built by Thomas Ham for Codaisseur
