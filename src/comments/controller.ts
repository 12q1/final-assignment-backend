// src/comments/controller.ts
import { JsonController, Get, Param, Put, Body, NotFoundError, Post, HttpCode } from 'routing-controllers'
import Comment from './entity'

@JsonController()
export default class CommentController {

    @Get('/comments/:id')
    async getComment(
        @Param('id') id: number
    ) {
        return await Comment.findOne(id)
    }  //end of /comments/:id endpoint

    @Get('/comments')
    async allComments() {
        const comments = await Comment.find()
        return { comments }
    } //end of /comments endpoint

    @Put('/comments/:id')
    async updateComment(
        @Param('id') id: number, //find the comment from id
        @Body() update: Partial<Comment>
    ) {
        const comment = await Comment.findOne(id)
        if (!comment) throw new NotFoundError('Cannot find comment')//if the comment doesn't exist throw error

        return Comment.merge(comment, update).save()//if does exist merge the update
    } //end of PUT /comments/:id endpoint

    @Post('/comments')
    @HttpCode(201)
    createComment(
        @Body() comment: Comment
    ) {
        return comment.save()
    }
}