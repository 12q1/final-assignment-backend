// src/events/entity.ts
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm'
import { BaseEntity } from 'typeorm/repository/BaseEntity'
import { IsString, MinLength } from 'class-validator'
import User from '../users/entity';
import Ticket from '../tickets/entity';

@Entity()
export default class Event extends BaseEntity {

  @PrimaryGeneratedColumn()
  id?: number

  @IsString()
  @MinLength(4)
  @Column('text', { nullable: false })
  name: string

  @IsString()
  @MinLength(10)
  @Column('text', { nullable: true })
  description?: string

  @Column('text', { nullable: true })
  image?: string

  @Column('text', { nullable: true })
  date?: string

  @IsString()
  @Column('text', { nullable: true })
  location?: string

  @ManyToOne(_ => User, user => user.events)
  user: User;

  @OneToMany(_ => Ticket, ticket => ticket.event, {eager:true})
  tickets: Ticket[]
}