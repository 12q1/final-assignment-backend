// src/events/controller.ts
import { JsonController, Get, Param, Put, Body, NotFoundError, Post, HttpCode, Authorized } from 'routing-controllers'
import Event from './entity'

@JsonController()
export default class EventController {

    @Get('/events/:id')
    async getEvent(
        @Param('id') id: number
    ) {
        return await Event.findOne(id)
    }  //end of /events/:id endpoint

    @Get('/events')
    async allEvents() {
        const events = await Event.find()
        return { events }
    } //end of /events endpoint

    @Put('/events/:id')
    async updateEvent(
        @Param('id') id: number, //find the event from id
        @Body() update: Partial<Event>
    ) {
        const event = await Event.findOne(id)
        if (!event) throw new NotFoundError('Cannot find event')//if the event doesn't exist throw error

        return Event.merge(event, update).save()//if does exist merge the update
    } //end of PUT /events/:id endpoint

    @Authorized()
    @Post('/events')
    @HttpCode(201)
    createEvent(
        @Body() event: Event
    ) {
        return event.save()
    }
}