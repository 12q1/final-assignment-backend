// src/tickets/controller.ts
import { JsonController, Get, Param, Put, Body, NotFoundError, Post, HttpCode } from 'routing-controllers'
import Ticket from './entity'

@JsonController()
export default class TicketController {

    @Get('/tickets/:id')
    async getTicket(
        @Param('id') id: number
    ) {
        return await Ticket.findOne(id)
    }  //end of /tickets/:id endpoint

    @Get('/tickets')
    async allTickets() {
        const tickets = await Ticket.find()
        return { tickets }
    } //end of /tickets endpoint

    @Put('/tickets/:id')
    async updateTicket(
        @Param('id') id: number, //find the ticket from id
        @Body() update: Partial<Ticket>
    ) {
        const ticket = await Ticket.findOne(id)
        if (!ticket) throw new NotFoundError('Cannot find ticket')//if the ticket doesn't exist throw error

        return Ticket.merge(ticket, update).save()//if does exist merge the update
    } //end of PUT /tickets/:id endpoint

    @Post('/tickets')
    @HttpCode(201)
    createTicket(
        @Body() ticket: Ticket
    ) {
        return ticket.save()
    }
}