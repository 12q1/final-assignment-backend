// src/tickets/entity.ts
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, BeforeInsert, OneToMany, AfterLoad, CreateDateColumn } from 'typeorm'
import { BaseEntity } from 'typeorm/repository/BaseEntity'
import { IsString, Length } from 'class-validator'
import Event from '../events/entity'
import User from '../users/entity';
import Comment from '../comments/entity'

@Entity()
export default class Ticket extends BaseEntity {

    @PrimaryGeneratedColumn()
    id?: number

    @Column()
    risk: number

    @IsString()
    @Length(4, 10)
    @Column('text', { nullable: false })
    name: string

    @IsString()
    @Column('text', { nullable: true })
    price?: string

    @CreateDateColumn()
    created?: Date

    @Column('text', { nullable: true })
    imgUrl?: string

    @Column('text', { nullable: true })
    description?: string

    @ManyToOne(_ => Event, event => event.tickets)
    event: Event;

    @ManyToOne(_ => User, user => user.tickets)
    user: User;

    @OneToMany(_ => Comment, comment => comment.ticket, {eager:true})
    comments: Comment[]

    @BeforeInsert()
    updateCreation(){
    this.created=new Date()
    this.risk=5  
    this.comments = []

    }

    @AfterLoad()
    async checkRisk(){
       
    }
}