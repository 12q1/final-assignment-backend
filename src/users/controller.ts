// src/users/controller.ts
import { JsonController, Get, Param, Put, Body, NotFoundError, Post} from 'routing-controllers'
import User from './entity'

@JsonController()
export default class UserController {

    @Get('/users/:id')
    async getUser(
        @Param('id') id: number
    ) {
        return await User.findOne(id)
    }  //end of /users/:id endpoint

    @Get('/users')
    async allUsers() {
        const users = await User.find()
        return { users }
    } //end of /users endpoint

    @Put('/users/:id')
    async updateUser(
        @Param('id') id: number, //find the user from id
        @Body() update: Partial<User>
    ) {
        const user = await User.findOne(id)
        if (!user) throw new NotFoundError('Cannot find user')//if the user doesn't exist throw error

        return User.merge(user, update).save()//if does exist merge the update
    } //end of PUT /users/:id endpoint

    @Post('/users')
    async createUser(
      @Body() user: User
    ) {
      const {password, ...rest} = user
      const entity = User.create(rest)
      await entity.setPassword(password)
      return entity.save()
    }
}