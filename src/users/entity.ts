// src/users/entity.ts
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm'
import { BaseEntity } from 'typeorm/repository/BaseEntity'
import { IsString, IsEmail, MinLength } from 'class-validator'
import { Exclude } from 'class-transformer'
import * as bcrypt from 'bcrypt'
import Event from '../events/entity'
import Ticket from '../tickets/entity';
import Comment from '../comments/entity';

@Entity()
export default class User extends BaseEntity {

    @PrimaryGeneratedColumn()
    id?: number

    @Column('text', { nullable: true })
    name?: string

    @IsEmail()
    @Column('text', { nullable: false })
    email: string


    @Column('text', { nullable: true })
    createdAt?: string

    @Column('text', { nullable: true })
    role?: string

    @Column('text', { nullable: true })
    location?: string

    @IsString()
    @MinLength(8)
    @Column('text', { nullable: false })
    @Exclude({ toPlainOnly: true })
    password: string

    async setPassword(rawPassword: string) {
        const hash = await bcrypt.hash(rawPassword, 10)
        this.password = hash
    }

    checkPassword(rawPassword: string): Promise<boolean> {
        return bcrypt.compare(rawPassword, this.password)
    }

    @OneToMany(_ => Event, event => event.user, {eager:true})
    events: Event[]

    @OneToMany(_ => Ticket, ticket => ticket.user, {eager:true})
    tickets: Ticket[]

    @OneToMany(_ => Comment, comment => comment.user, {eager:true})
    comments: Comment[]

}